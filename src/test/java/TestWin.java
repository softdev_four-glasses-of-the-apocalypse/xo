
import com.mycompany.xo_program.XOgame;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ASUS
 */
public class TestWin {

    public TestWin() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void test_isWinOnRow_True() {
        XOgame.createTable();
        XOgame.testTableTrue();
        boolean result = XOgame.isWinOnRow();
        assertTrue(result);
    }

    @Test
    public void test_isWinOnCol_True() {
        XOgame.createTable();
        XOgame.testTableTrue();
        boolean result = XOgame.isWinOnCol();
        assertTrue(result);
    }

    @Test
    public void test_isWinOnCross_True() {
        XOgame.createTable();
        XOgame.testTableTrue();
        boolean result = XOgame.isWinOnCross();
        assertTrue(result);
    }

    @Test
    public void test_isWin_True() {
        XOgame.createTable();
        XOgame.testTableTrue();
        boolean result = XOgame.isWin();
        assertTrue(result);
    }

    @Test
    public void test_isWinOnRow_False() {
        XOgame.createTable();
        XOgame.testTableFalse();
        boolean result = XOgame.isWinOnRow();
        assertFalse(result);
    }

    @Test
    public void test_isWinOnCol_False() {
        XOgame.createTable();
        XOgame.testTableFalse();
        boolean result = XOgame.isWinOnCol();
        assertFalse(result);
    }

    @Test
    public void test_isWinOnCross_False() {
        XOgame.createTable();
        XOgame.testTableFalse();
        boolean result = XOgame.isWinOnCross();
        assertFalse(result);
    }

    @Test
    public void test_isWin_False() {
        XOgame.createTable();
        XOgame.testTableFalse();
        boolean result = XOgame.isWin();
        assertFalse(result);
    }

    @Test
    public void test_isDraw_True() {
        XOgame.createTable();
        XOgame.testTableFalse();
        boolean result = XOgame.isDraw();
        assertTrue(result);
    }

    @Test
    public void test_isDraw_False() {
        XOgame.createTable();
        boolean result = XOgame.isDraw();
        assertFalse(result);
    }
}
