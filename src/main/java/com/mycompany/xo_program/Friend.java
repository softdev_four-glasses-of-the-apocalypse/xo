package com.mycompany.xo_program;

import java.io.Serializable;

public class Friend implements Serializable{

    private int id;
    private String name;
    private int age;
    private String tel;
    private static int lastId = 1;

    public Friend(String name, int age, String tel) {
        this.id = lastId++;
        this.name = name;
        this.age = age;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age == 0) {
            return;
        }
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Friend{" + "id=" + id + ", name=" + name + ", age=" + age + ", tel=" + tel + '}';
    }

     
}
