
package com.mycompany.xo_program;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestWriteFriend {

    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Friend f1 = new Friend("RickRoll", 99, "0999999999");
            Friend f2 = new Friend("Dio", 3000, "1234567890");
            System.out.println(f1);
            System.out.println(f2);
            File file = new File("friend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(f1);
            oos.writeObject(f2);
            oos.close();
            fos.close();
        } catch (Exception ex) {
            System.out.println("Error!!!");
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
