package com.mycompany.xo_program;

import java.io.Serializable;

public class Player implements Serializable{

    private char symbol;
    private int winCount, loseCount, drawCount;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDrawCount() {
        return drawCount;
    }

    public void win() {
        winCount++;
    }

    public void lose() {
        loseCount++;
    }

    public void draw() {
        drawCount++;
    }

    @Override
    public String toString() {
        return "Player{symbol=" + symbol + ", winCount=" + winCount + ", loseCount=" + loseCount + ", drawCount=" + drawCount; 
    }
    
    
}
