
import com.mycompany.xo_program.XOgame;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestAdd {
    
    public TestAdd() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void test_Add_Num1_1_Num2_6_Output_7() {
        int result = XOgame.AddNum(1, 6);
        assertEquals(7, result);
    }
    
    @Test
    public void test_Add_Num1_4_Num2_6_Output_10() {
        int result = XOgame.AddNum(4, 6);
        assertEquals(10, result);
    }
    
    
}
